//$(document).ready(function() {
//    $('#currency').DataTable( {
//        ajax: {
//            type : "POST",
//            url: "api/curs",
//            dataSrc:"",
//            data: {}
//        },
//        columns: [
//            {data:"cur"},
//            {data:"buying"},
//            {data:"selling"}
//        ],
//        paging: false,
//        searching: false,
//        info: false,
//        ordering: false,
//    } );
//} );

function requestCurTable(){
    var code = $("#booth").val();
    var rows = $("#rows").val();
    var pins = $("#pins").val();
    $.ajax({
        type : "POST",
        url : "/fxbooth/api/cursTable",
        data: {"code":code,"rows":rows,"pins":pins},
        dataType:"json",
        async:false,
        success : function(data){
            $("#curs-body").html(data["htmlTable"]);
            $("#page-body").html(numPage+"/"+data["maxPage"]);
            maxPage = parseInt(data["maxPage"]);
            date = data["date"];
            time = data["time"];
            renderCurTable(numPage);
            renderEmptyTable();
        },
        error: function(a,b,c){
            console.log(a,b,c);
        }
    });
}
function renderCurTable(page){
    $("tr[class^='page-']").hide();
    $(".page-"+page).show();
    $("#date-body").val(date);
    $("#time-body").val(time);
    $("#page-body").val(numPage+"/"+maxPage);
}
function renderEmptyTable(){
    var heightCur = $("table[id=currency] tbody tr").eq(0).css("height");
    $(".empty-tr td").css("height",heightCur)
}

var numPage = "1";
var maxPage = 1;
var date = "";
var time = "";

function changePage(){
    var nextPage = parseInt(numPage) + 1;
    if(nextPage<=maxPage){
        numPage = "" + nextPage;
        renderCurTable(""+nextPage);
    }else{
        numPage = "1"
        renderCurTable(numPage);
    }
}

var stompClient = null;

function setConnected(connected) {
    $("#connect").prop("disabled", connected);
    $("#disconnect").prop("disabled", !connected);
    if (connected) {
        $("#conversation").show();
    }
    else {
        $("#conversation").hide();
    }
    $("#greetings").html("");
}

function connect() {
    var socket = new SockJS('/fxbooth/rate-websocket');
    stompClient = Stomp.over(socket);
    stompClient.connect({}, function (frame) {
        setConnected(true);
        console.log('Connected: ' + frame);
        stompClient.subscribe('/topic/greetings', function (greeting) {
//            showGreeting(JSON.parse(greeting.body).content);
            if(JSON.parse(greeting.body).status == "Reloading"){
                console.log(JSON.parse(greeting.body).date);
                requestCurTable();
            }
        });
    });
}

function disconnect() {
    if (stompClient !== null) {
        stompClient.disconnect();
    }
    setConnected(false);
    console.log("Disconnected");
}

function sendName() {
    stompClient.send("/app/hello", {}, JSON.stringify({'name': $("#name").val()}));
}

function showGreeting(message) {
    $("#greetings").append("<tr><td>" + message + "</td></tr>");
}

$(function () {
    connect();
    requestCurTable();
    setInterval(function(){ changePage(); }, 10000);
    /*$("form").on('submit', function (e) {
        e.preventDefault();
    });
    $( "#connect" ).click(function() { connect(); });
    $( "#disconnect" ).click(function() { disconnect(); });
    $( "#send" ).click(function() { sendName(); });*/
});

$(window).resize(function() {
    var heightFoot = $("#fx-footer").height();
    $(".main-body").css("padding-bottom",heightFoot);
});
