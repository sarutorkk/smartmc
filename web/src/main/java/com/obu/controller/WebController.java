package com.obu.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpSession;

import static org.springframework.web.bind.annotation.RequestMethod.GET;

@Controller
public class WebController {

    @ResponseBody
    @RequestMapping(value = "/", method = GET)
    public ModelAndView home(HttpSession session) throws Exception{
        ModelAndView view = new ModelAndView("index");
        return view;
    }
    @ResponseBody
    @RequestMapping(value = "/tree", method = GET)
    public ModelAndView tree(HttpSession session) throws Exception{
        ModelAndView view = new ModelAndView("tree");
        return view;
    }
    @ResponseBody
    @RequestMapping(value = "/tib", method = GET)
    public ModelAndView tib(HttpSession session) throws Exception{
        ModelAndView view = new ModelAndView("tib");
        return view;
    }
    @ResponseBody
    @RequestMapping(value = "/tib2", method = GET)
    public ModelAndView tib2(HttpSession session) throws Exception{
        ModelAndView view = new ModelAndView("tib2");
        return view;
    }

}
