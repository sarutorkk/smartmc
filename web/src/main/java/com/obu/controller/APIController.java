package com.obu.controller;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpSession;

import java.util.HashMap;
import java.util.Map;

import static org.springframework.web.bind.annotation.RequestMethod.GET;

@RestController
@RequestMapping("api")
public class APIController {

    @ResponseBody
    @RequestMapping(value = "/tree", method = GET,produces = MediaType.APPLICATION_JSON_VALUE)
    public Map<String,Object> tree(HttpSession session) throws Exception{
        Map<String,Object> map = new HashMap<>();
        map.put("body","tree");
        map.put("header","head");
        return map;
    }
}
